/**
 * @index.js - manages all routing
 *
 * router.get when assigning to a single request
 * router.use when deferring to a controller
 *
 * @requires express
 */

const express = require('express')
const LOG = require('../utils/logger.js')

LOG.debug('START routing')
const router = express.Router()

// Manage top-level request first
router.get('/', (req, res, next) => {
  LOG.debug('Request to /')
  res.render('index.ejs', { title: 'Express App' })
})

// Defer path requests to a particular controller
router.use('/about', require('../controllers/about.js'))
router.use('/flight', require('../controllers/flights.js'))
router.use('/plane', require('../controllers/planes.js'))
router.use('/pilot', require('../controllers/pilots.js'))

router.use('/mypage', require('../controllers/mypage.js'))
router.use('/mypage1', require('../controllers/mypage1.js'))
router.use('/stats', require('../controllers/stats.js'))

//router.use('/stats',require('../controllers/stats.js'))

LOG.debug('END routing')
module.exports = router
